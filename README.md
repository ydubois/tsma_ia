# TSMA_IA

## Scores 

- Random : 0.13666
- Knn : 0.33182
- Knn avec features : 0.43469
    - À noter que dans ce cas en utilisant un nombre de neighboor plus faible nous obtenons un meilleur résultat.
- Catboost avec features : 0.63640
- Neural Network avec features : 0.60363
- Catboost VGGish = 0.6712
- Deep VGGish = 0.60413
- CNN melspec = 0.35653